package edu.cyut.csie;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.printf("輸入身分證\n");
        String num1 = scanner.next();
        if (num1.matches("[A-Z]{1}[0-9]{9}"))
            System.out.print("驗證成功\n" + num1);
        else
            System.out.print("請輸入身分證");
    }
}